FROM python:3.10 as builder

# Set working directory
WORKDIR /app

# Copy only the requirements file to leverage Docker cache
COPY requirements.txt .

# Create and activate virtual environment
RUN python -m venv venv
RUN /bin/bash -c "source venv/bin/activate"

# Install dependencies
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

# Copy the rest of the application code
COPY . /app

# Your further Dockerfile instructions...

# Command to run your application
CMD ["python", "src\training_pipeline\run_training_pipeline.py"]
