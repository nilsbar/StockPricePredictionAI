from utils.pipeline_node import PipelineNode

class ModelQualityValidation(PipelineNode):
    """
    A class for the Validation of a model.
    """
    def __init__(self) -> None:
        super().__init__()

    def process(self, input):
        """
        Hits the process for model validation.

        Parameters:

        input: model

        Return:

        result (dict): {model score: (model, score)}.
        """
        # get the data for validation
        # validation process
        pass

    def _datagatherung(self):
        pass