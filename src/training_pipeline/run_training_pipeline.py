from src.training_pipeline.data_preprocessing import DataPreprocessing
from src.training_pipeline.hyperparameter_optimization import HyperParameterOptimization
from src.utils.pipeline import Pipeline
from src.utils.mlflow_logger import Modellogger
from src.models.varma_model import VARMAModel

data_preprocessing = DataPreprocessing()
# adjust the algorithm under variable "model_scheme"
hyperparameter_optimization = HyperParameterOptimization(model_scheme="varma")

steps = [data_preprocessing, hyperparameter_optimization]
pipeline = Pipeline(steps=steps)

result_model = pipeline.run()

model_logger = Modellogger()

model_logger.log_model_statsmodels(model=result_model, mlflow_tag="varma")

"""
Tags for the models:

VarMa: "varma"

"""
