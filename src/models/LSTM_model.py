import torch
import torch.nn as nn
from src.models.base_model import BaseModel


class LSTMModel(BaseModel):
    def __init__(self) -> None:
        super().__init__()

    def train(self, parameters: dict):
        """ """
        super().train(parameters=parameters)

    def predict():
        pass

class TorchLSTMimplementation(nn.Module):

    def __init__(self):
        super().__init__()
        self.lstm = nn.LSTM(input_size=1, hidden_size=50, num_layers=1, batch_first=True)
        self.linear = nn.Linear(50, 1)
    def forward(self, x):
        x, _ = self.lstm(x)
        x = self.linear(x)
        return x