import mlflow

class Modellogger:

    def __init__(self):
        pass

    def pull_model_statsmodels(self, model: any, mlflow_tag: str):
        
        mlflow.statsmodels.log_model(model, artifact_path=None,  registered_model_name= mlflow_tag)