import mlflow

class Modellogger:

    def __init__(self):
        pass

    def log_model_statsmodels(self, model: any, mlflow_tag: str):
        """
        This function logs specifically statsmodel - models into the model registry.
        """
        mlflow.statsmodels.log_model(model, artifact_path=None,  registered_model_name= mlflow_tag)

    def log_model_sklearn(self):
        """
        This function logs specifically scikit-learn - models into the model registry.
        """
        pass

class ExperimentLogger:
    pass